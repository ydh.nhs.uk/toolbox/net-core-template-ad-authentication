﻿using System;
using System.Collections.Generic;

namespace ADAuthTemplate.Models
{
    public partial class User
    {
        //public User()
        //{
        //    UserRole = new HashSet<UserRole>();
        //}

        public string Id { get; set; }
        public string UserName { get; set; }
        public string NormalizedEmail { get; set; }
        public string SecurityStamp { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string Password { get; set; }

        public virtual ICollection<UserRole> UserRole { get; set; }
    }
}
