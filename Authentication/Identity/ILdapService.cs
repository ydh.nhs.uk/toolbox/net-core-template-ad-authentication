﻿using ADAuthTemplate.Models;

namespace ADAuthTemplate.Identity
{
    public interface ILdapService
    {
        bool Authenticate(string SamAccountName, string password);
        public User GetUserByUserName(string userName);
    }
}
