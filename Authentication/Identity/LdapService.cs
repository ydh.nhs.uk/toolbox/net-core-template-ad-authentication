﻿using ADAuthTemplate.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Principal;

namespace ADAuthTemplate.Identity
{
    public class LdapService : ILdapService
    {
        private readonly ApplicationContext _db;
        private readonly IWebHostEnvironment _env;
        private readonly ILogger<LdapService> _logger;

        public LdapService(ApplicationContext db, IWebHostEnvironment env, ILogger<LdapService> logger)
        {
            _db = db;
            _env = env;
            _logger = logger;
        }

        public bool Authenticate(string SamAccountName, string password)
        {
            if (_env.EnvironmentName == "Development") return true; // Authenticate any password in development mode!

            if (SamAccountName.StartsWith("test.")) // Authenticate any test logins
            {
                return true;
            }
            else
            {
                using PrincipalContext principalContext = new PrincipalContext(ContextType.Domain, "YDH");
                return principalContext.ValidateCredentials(SamAccountName, // + "@YDH.NHS.UK", 
                                                                        password,
                                                                        ContextOptions.Negotiate |
                                                                        ContextOptions.Signing |
                                                                        ContextOptions.Sealing);
            }
        }

        public User GetUserByUserName(string userName)
        {
            try
            {
                using PrincipalContext principalContext = new PrincipalContext(ContextType.Domain, "YDH");
                var userPrincipal = UserPrincipal.FindByIdentity(principalContext, IdentityType.SamAccountName, userName);

                return this.CreateUserFromAttributes(userName, userPrincipal);
            }
            catch (PrincipalServerDownException ex)
            {
                _logger.LogError(ex, "Error contacting AD server");
                return null;
            }
        }

        private User CreateUserFromAttributes(string userName, UserPrincipal principal)
        {
            if (principal == null && !userName.StartsWith("test.")) return null;

            var ldapUser = _db.User
                            .Where(w => w.UserName == userName)
                            .Include(i => i.UserRole)
                            .ThenInclude(t => t.Role)
                            .FirstOrDefault();

            if (ldapUser == null)
            {
                ldapUser = new User
                {
                    UserName = principal.SamAccountName,
                    NormalizedEmail = principal.EmailAddress
                };
                _db.User.Add(ldapUser);
                _db.SaveChanges();
            }

            // Get the AD groups
            List<string> tokens = new List<string>();
            if (!userName.StartsWith("test."))
            {
                DirectorySearcher ds = new DirectorySearcher();
                ds.Filter = String.Format("(&(objectClass=user)(sAMAccountName={0}))", userName);
                SearchResult sr = ds.FindOne();

                DirectoryEntry user = sr.GetDirectoryEntry();
                user.RefreshCache(new string[] { "tokenGroups" });

                for (int i = 0; i < user.Properties["tokenGroups"].Count; i++)
                {
                    SecurityIdentifier sid = new SecurityIdentifier((byte[])user.Properties["tokenGroups"][i], 0);
                    NTAccount nt = (NTAccount)sid.Translate(typeof(NTAccount));
                    tokens.Add(nt.Value);
                }
            }

            ldapUser.MemberOf = tokens.ToArray();

            return ldapUser;
        }
    }
}
