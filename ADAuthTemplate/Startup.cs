using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using AspNetCoreRateLimit;
using Joonasw.AspNetCore.SecurityHeaders;
using ADAuthTemplate.Configuration;
using ADAuthTemplate.Identity;
using ADAuthTemplate.Models;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace ADAuthTemplate
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            Environment = environment;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("ApplicationConnection"),
                    o => o.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery)));

            services.AddExceptional(Configuration.GetSection("Exceptional"), settings =>
            {
                settings.UseExceptionalPageOnThrow = Environment.IsDevelopment();
            });

            services.AddDefaultIdentity<User>()
                .AddRoles<Role>()
                .AddUserManager<LdapUserManager>()
                .AddRoleStore<LdapRoleStore>()
                .AddUserStore<LdapUserStore>()
                .AddSignInManager<LdapSignInManager>()
                .AddClaimsPrincipalFactory<LdapUserClaimsPrincipalFactory>();

            services.AddCsp(nonceByteAmount: 32);

            services.AddDistributedMemoryCache();

            services.AddTransient<ILdapService, LdapService>();
            services.AddTransient<IUserStore<User>, LdapUserStore>();
            services.AddTransient<IRoleStore<Role>, LdapRoleStore>();

            //services.AddTransient<IEmailService, EmailService>();

            services.AddMvc(options => options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute()))
                .AddSessionStateTempDataProvider()
                .AddRazorRuntimeCompilation();

            services.AddResponseCompression();

            // IP rate limting
            services.AddOptions();
            services.AddMemoryCache();
            services.Configure<IpRateLimitOptions>(Configuration.GetSection("IpRateLimiting"));
            services.AddSingleton<IIpPolicyStore, MemoryCacheIpPolicyStore>();
            services.AddSingleton<IRateLimitCounterStore, MemoryCacheRateLimitCounterStore>();
            services.AddSingleton<IRateLimitConfiguration, RateLimitConfiguration>();
            services.AddHttpContextAccessor();

            services.Configure<ApplicationOptions>(Configuration.GetSection("ApplicationOptions"));

            services.Configure<CookiePolicyOptions>(opts =>
            {
                opts.CheckConsentNeeded = _ => true;
                opts.HttpOnly = Microsoft.AspNetCore.CookiePolicy.HttpOnlyPolicy.Always;
                opts.Secure = Microsoft.AspNetCore.Http.CookieSecurePolicy.Always;
                opts.MinimumSameSitePolicy = Microsoft.AspNetCore.Http.SameSiteMode.Strict;
            });

            services.AddSession(opts =>
            {
                opts.Cookie.IsEssential = true;
                opts.Cookie.HttpOnly = true;
                opts.Cookie.SecurePolicy = Microsoft.AspNetCore.Http.CookieSecurePolicy.Always;
                opts.Cookie.SameSite = Microsoft.AspNetCore.Http.SameSiteMode.Strict;
            });

            services.AddAuthorization(options => options.AddPolicy("TestPolicy", policy => policy.RequireClaim("SQLDBA")));

            services.AddDatabaseDeveloperPageExceptionFilter();

            // Uncomment/edit this to set the session timeout interval
            //services.Configure<SecurityStampValidatorOptions>(options =>
            //    options.ValidationInterval = TimeSpan.FromMinutes(1));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts(new Joonasw.AspNetCore.SecurityHeaders.HstsOptions
                {
                    IncludeSubDomains = true // Uses Joonasw.AspNetCore.SecurityHeaders
                });
            }

            app.UseExceptional();
            app.UseIpRateLimiting();
            app.UseResponseCompression();

            app.UseCsp(csp =>
            {
                csp.AllowScripts
                        .FromSelf()
                        .AddNonce()
                        .From("kit.fontawesome.com")
                        .From("cdn.datatables.net")
                        .From("ajax.aspnetcdn.com")
                        .AllowUnsafeInline(); // NOTE - this will be IGNORED by browsers that recognise the nonce but is needed for Safari compatibility
                csp.AllowStyles
                        .FromSelf()
                        .From("kit-free.fontawesome.com")
                        .From("cdn.datatables.net")
                        .From("ajax.aspnetcdn.com")
                        .AllowUnsafeInline(); // Needed for font-awesome
                csp.AllowImages
                        .FromSelf()
                        .From("data:"); // For SVGs
                csp.ByDefaultAllow
                        .FromSelf();
                csp.AllowFormActions
                        .ToSelf();
                csp.AllowFraming
                        .FromNowhere();
            }); // Content-Security-Policy, uses Joonasw.AspNetCore.SecurityHeaders

            app.UseHttpsRedirection();

            // Set a max-age header for static assets
            // Note that this won't stop site.js/site.css from updating correctly
            // as they are referenced with asp-append-version="true"
            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = ctx =>
                {
                    const int durationInSeconds = 60 * 60 * 24 * 30; // 30 days
                    ctx.Context.Response.Headers[HeaderNames.CacheControl] =
                        "public,max-age=" + durationInSeconds;
                }
            });

            app.UseCookiePolicy();
            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseSession(new SessionOptions()
            {
                Cookie = new CookieBuilder()
                {
                    Name = ".AspNetCore.Session.ApplicationName" // *********** change this to name of application
                }
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }
    }
}
