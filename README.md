## Description

This is a Visual Studio template for C#/.NET Core applications that need to use
active directory to authenticate their users. It's purpose is to speed up development
of future applications.

## Features

* Customised Identity classes that use AD authentication
  * Support for database-defined roles and policies derived from AD groups
  * All unecessary/unused pages removed
  * EF context and POCO classes already scaffolded and ready to go
  * Script to create database tables included
* NuGet packages included
  * All relevant EntityFramework packages
  * Joonasw.AspNetCore.SecurityHeaders
  * AspNetCoreRateLimit
  * Windows.Compatibility
  * NLog
* Client-side libs included
  * JQuery
  * Bootstrap 4
  * Font awesome
  * Toastr
  * better-dateinput-polyfill (for IE)
* nlog.config file to configure logging with rolling logs
* web.config file to set up security/HTTP headers
* TypeScript support
* Sample appsettings.*.json files
* Sample ApplicationOptions class
* Startup.cs class fully configured including
  * IP rate limiting
  * CSP policy
  * Enabling nonce for inline scripts
  * Caching policy for static files
  * Automatic verification of anti-forgery tokens
* _Layout.cshtml and _LoginPartial.cshtml views
  * Set up correctly with sample navbar links
  * YDH branding/colours
  * HTTP meta headers and favicon set up
  * All relevant js/css files included
* .gitignore file configured correctly
* Now have separate pages for main project, authentication code and authentication pages
* Nullable reference types enabled for main project


Some work will be required to enable external (non-AD) logins, if required

## Exporting as a project template

The solution has now been split into multiple projects, this has been done for several reasons. Firstly it separates the main project code from the authentication logic and authentication UI. Secondly it was done to enable [nullable reference types](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/nullable-reference-types) to be enabled - and as EF Core doesn't support nullable reference types yet, all of the database model classes need to be in a separate project that doesn't have nullable reference types enabled. 

For instructions on how to export a multi-project solution as a template, see:

[https://docs.microsoft.com/en-us/visualstudio/ide/how-to-create-multi-project-templates?view=vs-2019](https://docs.microsoft.com/en-us/visualstudio/ide/how-to-create-multi-project-templates?view=vs-2019)
